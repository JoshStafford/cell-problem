image = [
 [0,0,0,0,0,0,0,0],
 [1,0,0,0,0,1,0,0],
 [1,1,1,0,1,1,1,0],
 [1,0,1,0,0,1,1,0],
 [1,1,1,0,0,0,0,0],
 [1,1,0,0,0,0,0,0],
 [0,0,0,0,0,0,1,1],
 [0,0,0,0,0,0,1,1]
 ]

 # new_neighbours = [[n[0]-1, n[1]+1], [n[0], n[1]+1], [n[0]+1, n[1]+1], [n[0]-1, n[1]], [n[0]+1, n[1]], [n[0]-1, n[1]-1], [n[0], n[1]-1], [n[0]+1,  n[1]-1]]
 # n[0] >= 0 and n[0] < width and n[1] >= 0 and n[1] < height

class Petri:


    def __init__(self, image):
        self.__image = image
        self.width = len(image[0])
        self.height = len(image)

    def cell_size(self, x, y):

        if(self.__image[y][x] == 0):
            return None
        
        neighbours = [[x-1, y+1], [x, y+1], [x+1, y+1], [x-1, y], [x+1, y], [x-1, y-1], [x, y-1], [x+1,  y-1]]
        found = []

        size = 0

        new_found = 1

        while new_found > 0:

            new_found = 0

            for pos in neighbours:

                if pos in neighbours and pos not in found:
                
                    try:
                        if(self.__image[pos[1]][pos[0]] == 1):
                            size += 1
                            new_found += 1
                            found.append(pos)
                        
                            new_neighbours = [[pos[0]-1, pos[1]+1], [pos[0], pos[1]+1], [pos[0]+1, pos[1]+1], [pos[0]-1, pos[1]], [pos[0]+1, pos[1]], [pos[0]-1, pos[1]-1], [pos[0], pos[1]-1], [pos[0]+1,  pos[1]-1]]

                            for new_pos in new_neighbours:

                                if new_pos not in neighbours:

                                    if new_pos[0] >= 0 and new_pos[0] < self.width and new_pos[1] >= 0 and new_pos[1] < self.height:
                                        neighbours.append(new_pos)
                    except:
                        pass

        result = {
            "size" : size,
            "neighbours" : neighbours
        }

        return result

    def count_cells(self):

        locations = []
        count = 0

        for y in range(self.height):

            for x in range(self.width):

                if(self.__image[y][x] == 0):
                    continue

                if([x, y] in locations):
                    continue
                
                neighbours = [[x-1, y+1], [x, y+1], [x+1, y+1], [x-1, y], [x+1, y], [x-1, y-1], [x, y-1], [x+1,  y-1]]
                found = []


                new_found = 1

                while new_found > 0:

                    new_found = 0

                    for pos in neighbours:

                        if pos in neighbours and pos not in found:
                        
                            try:
                                if(self.__image[pos[1]][pos[0]] == 1):
                                    new_found += 1
                                    found.append(pos)
                                
                                    new_neighbours = [[pos[0]-1, pos[1]+1], [pos[0], pos[1]+1], [pos[0]+1, pos[1]+1], [pos[0]-1, pos[1]], [pos[0]+1, pos[1]], [pos[0]-1, pos[1]-1], [pos[0], pos[1]-1], [pos[0]+1,  pos[1]-1]]

                                    for new_pos in new_neighbours:

                                        if new_pos not in neighbours:

                                            if new_pos[0] >= 0 and new_pos[0] < self.width and new_pos[1] >= 0 and new_pos[1] < self.height:
                                                neighbours.append(new_pos)
                            except:
                                pass

                count += 1
                for pos in neighbours:
                    locations.append(pos)

        return count

    def label_cells(self):

        locations = []
        count = 2

        labelled_image = []

        for i in range(self.height):
            labelled_image.append([])
            for j in range(self.width):
                labelled_image[i].append(0)

        for y in range(self.height):

            for x in range(self.width):

                if(self.__image[y][x] == 0):
                    continue

                if([x, y] in locations):
                    continue
                
                neighbours = [[x-1, y+1], [x, y+1], [x+1, y+1], [x-1, y], [x+1, y], [x-1, y-1], [x, y-1], [x+1,  y-1]]
                found = []


                new_found = 1

                while new_found > 0:

                    new_found = 0

                    for pos in neighbours:

                        if pos in neighbours and pos not in found:
                        
                            try:
                                if(self.__image[pos[1]][pos[0]] == 1):
                                    labelled_image[pos[1]][pos[0]] = count
                                    new_found += 1
                                    found.append(pos)
                                
                                    new_neighbours = [[pos[0]-1, pos[1]+1], [pos[0], pos[1]+1], [pos[0]+1, pos[1]+1], [pos[0]-1, pos[1]], [pos[0]+1, pos[1]], [pos[0]-1, pos[1]-1], [pos[0], pos[1]-1], [pos[0]+1,  pos[1]-1]]

                                    for new_pos in new_neighbours:

                                        if new_pos not in neighbours:

                                            if new_pos[0] >= 0 and new_pos[0] < self.width and new_pos[1] >= 0 and new_pos[1] < self.height:
                                                neighbours.append(new_pos)
                            except:
                                pass

                count += 1
                for pos in neighbours:
                    locations.append(pos)

        return labelled_image

    def get_average_cell(self):

        locations = []
        sizes = []

        for y in range(self.height):

            for x in range(self.width):

                if([x, y] in locations):
                    continue

                cell = self.cell_size(x, y)

                if cell is not None:
                    sizes.append(cell["size"])
                    
                    for pos in cell["neighbours"]:
                        locations.append(pos)

        average = 0

        print(sizes)

        for val in sizes:
            average += val
        
        return average / len(sizes)

    def show_image(self):

        for row in self.__image:
            print(row)

p = Petri(image)